export enum FEATURE_FLAGS {
  CHATBOT = 'chatbot',
  SUBJECTS_DATABASE_HYDRATION = 'subjects-database-hydration',
  TRAINING_VIEW_DATABASE_HYDRATION = 'training-view-database-hydration',
  DB_CERT_UNLOCKING = 'db-cert-unlocking',
}
